<h1 align="center">
    <a href="https://gitlab.com/PedroCalheiros/portariaifsp">Sistema de Controle de Acesso IFSP</a>
</h1>
<p align="center">Aplicação para controle de acesso ao IFSP - Bragança Paulista-</p>

<p align="center">
 <a href="#Features">Features</a> • 
 <a href="#Pré-requisitos">Pré-requisitos</a> • 
 <a href="#rodando">Rodando</a> • 
 <a href="#autor">Autor</a> • 
 <a href="#licenc-a">Licença</a> • 
</p>

<h4 align="center"> 
	🚧  Teste Beta  🚧
</h4>

### Features Para Usuário Comum

- [x] Inserção de Visitantes
- [x] Busca por Parâmetros:
    - [x] Nome
    - [x] Documento
    - [x] Data
    - [x] Todos os Visitantes

### 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [Node.js](https://nodejs.org/en/)
- [React](https://pt-br.reactjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [MongoDB](https://www.mongodb.com/)

### Autor
---

<a href="https://gitlab.com/PedroCalheiros">
 <img style="border-radius: 50%;" src="https://gitlab.com/uploads/-/system/user/avatar/9433524/avatar.png?width=400" width="100px;" alt=""/>
 <br />
 <sub><b>Pedro Calheiros</b></sub></a> <a href="https://gitlab.com/PedroCalheiros" title="IFSP">🤠</a>


Feito com ❤️ por Pedro Calheiros 👋🏽 Entre em contato!

[![Twitter Badge](https://img.shields.io/badge/-@pe_calheiros-1ca0f1?style=flat-square&labelColor=1ca0f1&logo=twitter&logoColor=white&link=https://twitter.com/pe_calheiros)](https://twitter.com/pe_calheiros) [![Linkedin Badge](https://img.shields.io/badge/-Pedro-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/pedro-calheiros-65a8b7152/)](https://www.linkedin.com/in/pedro-calheiros-65a8b7152/) 
[![Gmail Badge](https://img.shields.io/badge/-pedrolcsle4@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:pedrolcsle4@gmail.com)](mailto:pedrolcsle4@gmail.com)

![Badge](https://img.shields.io/apm/l/vim-mode?style=flat-square)
